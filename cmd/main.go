package main

import (
	"fmt"
	"net"

	mailservice "bitbucket.org/losaped/mail-service"
	"bitbucket.org/losaped/mail-service/service"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

func main() {
	l, err := net.Listen("tcp", fmt.Sprintf(":%d", 3131))
	if err != nil {
		log.Fatalf("error on create listener: %v", err)
	}

	rpcSvr := grpc.NewServer()
	mailservice.RegisterMailServiceServer(rpcSvr, &service.MailService{})
	log.Error(rpcSvr.Serve(l))
}
