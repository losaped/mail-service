package service

import (
	"context"

	mservice "bitbucket.org/losaped/mail-service"
	"github.com/golang/protobuf/ptypes/empty"
)

type MailService struct{}

func (ms *MailService) Send(ctx context.Context, r *mservice.SendMessageRequest) (*empty.Empty, error) {
	return &empty.Empty{}, nil
}
