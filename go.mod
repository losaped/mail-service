module bitbucket.org/losaped/mail-service

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/grpc v1.21.0
)
