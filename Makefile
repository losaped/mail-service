gen:
	protoc --go_out=plugins=grpc:. proto/*

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .
